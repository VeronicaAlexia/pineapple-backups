package _struct

type Command struct {
	Download   string
	Search     string
	ShowConfig bool
	Update     bool
}
