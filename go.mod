module sf

go 1.19

require (
	github.com/gabriel-vasile/mimetype v1.4.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/gookit/color v1.5.1
	github.com/mattn/go-runewidth v0.0.13
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db
	github.com/tidwall/gjson v1.14.3
	github.com/tidwall/sjson v1.2.5
	github.com/vincent-petithory/dataurl v1.0.0
	golang.org/x/crypto v0.0.0-20220817201139-bc19a97f63c8
)

require (
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
)
